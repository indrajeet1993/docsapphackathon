import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import App from "./app";
import { routeSetup } from "react-hash-route";

function render() {
  ReactDOM.render(<App />, document.getElementById("root"));
}

routeSetup(render);
