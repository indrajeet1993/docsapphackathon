import React, { Component, PropTypes } from "react";
import { Container, Row, Col } from "reactstrap";
import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
import MenuItem from "material-ui/MenuItem";
import Checkbox from "material-ui/Checkbox";
import axios from "axios";
import ReactAudioPlayer from "react-audio-player";

class MediaWrapper extends Component {
  static propTypes = {
    className: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      showDialog: false,
      currentTrack: {},
      currentTrackInQueue: {}
    };
  }
  componentDidMount() {
    if (localStorage.user != undefined && localStorage.user != null) {
      this.setState({ user: JSON.parse(localStorage.user), isLoggedIn: true });
    }
  }
  componentWillMount() {
    if (localStorage.user != undefined && localStorage.user != null) {
      this.setState({ user: JSON.parse(localStorage.user), isLoggedIn: true });
    }
  }
  showPlaylistModal = (track, event) => {
    console.log(track, event);
    this.setState({ showDialog: true, currentTrack: track });
  };
  handleDialogClose = () => {
    this.setState({ showDialog: false, currentTrack: {} });
  };
  selectPlaylist = event => {
    event.preventDefault();
    event.stopPropagation();
    let payload = {
      playlist_id: event.target.getAttribute("data-value"),
      songInfo: JSON.stringify(this.state.currentTrack),
      song_id: this.state.currentTrack.id,
      self: true
    };
    if (event.target.checked) {
      payload["action"] = "add";
    } else {
      payload["action"] = "remove";
    }
    let self = this;
    axios
      .post("http://localhost:5000/addSongToPlaylist", { ...payload })
      .then(function(response) {
        self.setState({ showDialog: false });
      })
      .catch(function(error) {
        console.log(error);
      });
  };
  selectSharedPlaylist = event => {
    event.preventDefault();
    event.stopPropagation();
    let payload = {
      playlist_id: event.target.getAttribute("data-value"),
      songInfo: JSON.stringify(this.state.currentTrack),
      song_id: this.state.currentTrack.id,
      self: false
    };
    if (event.target.checked) {
      payload["action"] = "add";
    } else {
      payload["action"] = "remove";
    }
    let self = this;
    axios
      .post("http://localhost:5000/addSongToSharedPlaylist", { ...payload })
      .then(function(response) {
        self.setState({ showDialog: false });
        console.log(response.data);
      })
      .catch(function(error) {
        console.log(error);
      });
  };
  playSong = (track, event) => {
    event.preventDefault();
    event.stopPropagation();
    document.querySelectorAll(".fa-pause").forEach(item => {
      item.classList.remove("fa-pause");
      item.classList.add("fa-play");
    });
    event.target
      .closest(".PlayBtn")
      .querySelector(".fas")
      .classList.remove("fa-play");
    event.target
      .closest(".PlayBtn")
      .querySelector(".fas")
      .classList.add("fa-pause");
    this.setState({ currentTrackInQueue: track });
  };

  render() {
    return (
      <div className="container tracksCntnr">
        {this.props.tracks.map(track => {
          return (
            <Row key={track.id}>
              <Col xs="2">
                <div style={{ position: "relative" }}>
                  <div
                    className="trackBoxDefault"
                    style={{
                      background: "url('assets/song_vector.png')"
                    }}
                  />
                  <div
                    className="trackBox"
                    style={{
                      background:
                        "url('" +
                        track.artwork_url +
                        "?client_id=" +
                        appId +
                        "')"
                    }}
                    key={track.id}
                  />
                </div>
              </Col>
              <Col xs="10">
                <div className="SongRow">
                  <div
                    className="PlayBtn"
                    onClick={this.playSong.bind(this, track)}
                  >
                    <i className="fas fa-play" />
                  </div>
                  <span>
                    <smallTxt>{track.user.username}</smallTxt>
                    <br />
                    {track.title}
                  </span>
                  <div>
                    <button
                      className="addToPlaylistLink"
                      onClick={this.showPlaylistModal.bind(this, track)}
                    >
                      Add To Playlist
                    </button>
                  </div>
                </div>
              </Col>
            </Row>
          );
        })}
        <Dialog
          modal={true}
          clasName="playListModal"
          open={this.state.showDialog}
          onRequestClose={this.handleDialogClose}
          contentStyle={{ maxHeight: "auto" }}
          actions={
            <FlatButton
              label="Cancel"
              primary={true}
              onClick={this.handleDialogClose}
            />
          }
        >
          <Row>
            <Col xs="6" className="playListCntnr">
              {this.props.playlists.owned.length > 0 ? (
                <div>
                  <h4>Your playlists</h4>
                  {this.props.playlists.owned.map(item => {
                    return (
                      <div key={Math.random()}>
                        <Checkbox
                          label={item.name}
                          data-value={item.id}
                          onCheck={this.selectPlaylist.bind(this)}
                        />
                      </div>
                    );
                  })}
                </div>
              ) : (
                <div>You haven't created any playlist yet</div>
              )}
            </Col>
            <Col xs="6" className="playListCntnr">
              {this.props.playlists.shared.length > 0 ? (
                <div>
                  <h4>Playlists shared with you</h4>
                  <div>
                    {this.props.playlists.shared.map(item => {
                      return (
                        <div key={Math.random()}>
                          <Checkbox
                            label={item.name}
                            data-value={item.id}
                            onCheck={this.selectSharedPlaylist.bind(this)}
                          />
                        </div>
                      );
                    })}
                  </div>
                </div>
              ) : (
                <div>You don't have any shared playlist yet</div>
              )}
            </Col>
          </Row>
        </Dialog>
        <div className="PlayerCntnr">
          <div className="player">
            <ReactAudioPlayer
              src={
                this.state.currentTrackInQueue.stream_url +
                "?client_id=ygqBemDJqqQfdRCROgK9y7WNppl7x95D"
              }
              autoPlay
              controls
              style={{ width: "100%" }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default MediaWrapper;
