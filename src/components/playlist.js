import React, { Component, PropTypes } from "react";
import { Container, Row, Col } from "reactstrap";
import TextField from "material-ui/TextField";
import AppBar from "material-ui/AppBar";
import { getHash, route, getHashParameters } from "react-hash-route";
import ReactAudioPlayer from "react-audio-player";

import axios from "axios";
import MediaWrapper from "./mediaWrapper";

class Playlist extends Component {
  static propTypes = {
    className: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      currPlaylist: getHashParameters()[0],
      tracks: [],
      currentTrack: {},
      currentPlaylist: {},
      addUserEmail: ""
    };
  }
  componentDidMount() {
    if (localStorage.user != undefined && localStorage.user != null) {
      this.setState({ user: JSON.parse(localStorage.user), isLoggedIn: true });
      var self = this;
      axios
        .get(
          "http://localhost:5000/getPlaylistInfo?user+_id=" +
            this.state.user.id +
            "&playlist_id=" +
            getHashParameters()[0]
        )
        .then(function(response) {
          for (var i in response.data.songs) {
            response.data.songs[i].songInfo = JSON.parse(
              response.data.songs[i].songInfo
            );
          }
          // console.log("playlist:", response.data);

          self.setState({
            tracks: response.data.songs,
            currentPlaylist: response.data.info
          });
        })
        .catch(function(error) {
          console.log(error);
        });
    }
  }
  componentWillMount() {
    if (localStorage.user != undefined && localStorage.user != null) {
      this.setState({ user: JSON.parse(localStorage.user), isLoggedIn: true });
    } else {
      window.location.href = "/#";
    }
  }

  gotoHome = event => {
    event.preventDefault();
    event.stopPropagation();
    window.location.href = "/#";
  };
  getContainerHeight = () => {
    return { height: document.body.offsetHeight };
  };

  playSong = (track, event) => {
    document.querySelectorAll(".fa-pause").forEach(item => {
      item.classList.remove("fa-pause");
      item.classList.add("fa-play");
    });
    event.target
      .closest(".PlayBtn")
      .querySelector(".fas")
      .classList.remove("fa-play");
    event.target
      .closest(".PlayBtn")
      .querySelector(".fas")
      .classList.add("fa-pause");
    this.setState({ currentTrack: track });
  };

  addUser = event => {
    event.preventDefault();
    event.stopPropagation();
    let payload = {
      playlist_id: this.state.currentPlaylist.id,
      name: this.state.currentPlaylist.name,
      sharer_user_id: this.state.user.id,
      email: this.state.addUserEmail
    };

    var self = this;
    axios
      .post("http://localhost:5000/addUserToPlaylist", { ...payload })
      .then(function(response) {
        console.log(response.data);
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  render() {
    console.log(this.state.currentPlaylist);
    return (
      <div>
        <AppBar
          className="header"
          style={{ background: "#b5190e" }}
          title={
            <Row>
              <Col xs="3">
                <span onClick={this.gotoHome.bind(this)}>
                  <img src="assets/PlayPal.png" width="40px" /> PlayPal
                </span>
              </Col>
            </Row>
          }
          iconStyleLeft={{ display: "none" }}
          iconElementRight={
            this.state.isLoggedIn ? (
              <div className="pull-right userInfo">
                <img className="userPic" src={this.state.user.profile_pic} />
                {this.state.user.name}
              </div>
            ) : (
              <i />
            )
          }
        />
        <Container>
          <Row>
            <Col xs="12 " className="">
              <div className="playlistInfo ">
                <h1 className="text-center">
                  <u>{this.state.currentPlaylist.name}</u>
                </h1>
                <h5 className="text-left">
                  This play list has {this.state.tracks.length} song(s)
                </h5>
                {this.state.currentPlaylist.user_id != null ? (
                  <Row>
                    <Col xs="12">
                      <TextField
                        defaultValue=""
                        onChange={e => {
                          this.setState({ addUserEmail: e.target.value });
                        }}
                        floatingLabelText="Enter your pal's email here to share"
                      />
                      <button
                        className="addUser"
                        onClick={this.addUser.bind(this)}
                      >
                        <i className="fas fa-plus" />
                      </button>
                    </Col>
                  </Row>
                ) : (
                  ""
                )}
              </div>
              <hr />
            </Col>
            <Col
              className="container tracksCntnr"
              style={this.getContainerHeight()}
            >
              {this.state.tracks.map(track => {
                return (
                  <Row key={track.songInfo.id}>
                    <Col xs="2">
                      <div style={{ position: "relative" }}>
                        <div
                          className="trackBoxDefault"
                          style={{
                            background: "url('assets/song_vector.png')"
                          }}
                        />
                        <div
                          className="trackBox"
                          style={{
                            background:
                              "url('" +
                              track.songInfo.artwork_url +
                              "?client_id=" +
                              appId +
                              "')"
                          }}
                          key={track.songInfo.id}
                        />
                      </div>
                    </Col>
                    <Col xs="10">
                      <div className="SongRow">
                        <div
                          className="PlayBtn"
                          onClick={this.playSong.bind(this, track.songInfo)}
                        >
                          <i className="fas fa-play" />
                        </div>
                        <span>
                          <smallTxt>{track.songInfo.user.username}</smallTxt>
                          <br />
                          {track.songInfo.title}
                        </span>
                      </div>
                    </Col>
                  </Row>
                );
              })}
            </Col>
          </Row>
        </Container>
        <div className="PlayerCntnr">
          <div className="player">
            <ReactAudioPlayer
              src={
                this.state.currentTrack.stream_url +
                "?client_id=ygqBemDJqqQfdRCROgK9y7WNppl7x95D"
              }
              autoPlay
              controls
              style={{ width: "100%" }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Playlist;
