import React, { Component, PropTypes } from "react";
import { GoogleLogin } from "react-google-login-component";
import { Container, Row, Col } from "reactstrap";
import AppBar from "material-ui/AppBar";
import RaisedButton from "material-ui/RaisedButton";
import axios from "axios";
import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
import MenuItem from "material-ui/MenuItem";
import TextField from "material-ui/TextField";

import "./styles.scss";
import MediaWrapper from "./mediaWrapper";

class Main extends Component {
  static propTypes = {
    className: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      isLoggedIn: false,
      user: {},
      searchparam: "",
      tracks: [],
      playlists: {
        owned: [],
        shared: []
      },
      showDialog: false,
      showPlaylistForm: false,
      newPlName: ""
    };
    this.responseGoogle = this.responseGoogle.bind(this);
    this.getTopTracks = this.getTopTracks.bind(this);
  }
  gotoHome = event => {
    event.preventDefault();
    event.stopPropagation();
    window.location.href = "/#";
  };

  componentDidMount() {
    if (localStorage.user != undefined && localStorage.user != null) {
      this.setState({ user: JSON.parse(localStorage.user), isLoggedIn: true });
      this.getTopTracks();
    }
  }
  componentWillMount() {
    if (localStorage.user != undefined && localStorage.user != null) {
      this.setState({ user: JSON.parse(localStorage.user), isLoggedIn: true });
    }
  }
  getTopTracks = () => {
    var self = this;
    axios
      .get("http://localhost:5000/getPlaylists?userId=" + this.state.user.id)
      .then(function(response) {
        self.setState({ playlists: response.data });
      })
      .catch(function(error) {
        console.log(error);
      });

    var page_size = 100;
    SC.get(
      "/tracks?client_id=ygqBemDJqqQfdRCROgK9y7WNppl7x95D&limit=100",
      {}
    ).then(function(tracks) {
      self.setState({ tracks });
    });
  };

  handleDialogOpen = () => {
    this.setState({ showDialog: true });
  };

  handleDialogClose = () => {
    this.setState({ showDialog: false });
  };

  responseGoogle(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    var googleId = googleUser.getId();
    var profile = googleUser.getBasicProfile();
    let payload = {
      name: profile.getName(),
      email: profile.getEmail(),
      token: id_token,
      profile_pic: profile.getImageUrl(),
      accountId: profile.getId()
    };
    var self = this;

    axios
      .post("http://localhost:5000/createUser", { ...payload })
      .then(function(response) {
        localStorage.setItem("user", JSON.stringify(response.data[0]));
        self.getTopTracks();
        self.setState({ user: response.data[0], isLoggedIn: true });
      })
      .catch(function(error) {
        console.log(error);
      });
    //anything else you want to do(save to localStorage)...
  }

  handleChange = event => {
    this.setState({ searchparam: event.target.value });
  };

  searchSongs = event => {
    event.preventDefault();
    event.stopPropagation();
    var self = this;
    if (this.state.isLoggedIn) {
      if (this.state.searchparam.length > 0) {
        SC.get("/tracks", {
          q: this.state.searchparam
        }).then(function(tracks) {
          self.setState({ tracks });
        });
      }
    }
  };

  showPlaylists = event => {
    event.preventDefault();
    event.stopPropagation();
    let payload = {
      userId: this.state.user.id
    };
    this.setState({ showDialog: true });
  };

  togglePlaylistForm = event => {
    event.preventDefault();
    event.stopPropagation();
    this.setState({
      showPlaylistForm: true
    });
  };

  createPlayList = event => {
    event.preventDefault();
    event.stopPropagation();
    let payload = {
      name: this.state.newPlName,
      user_id: this.state.user.id
    };
    let self = this;
    axios
      .post("http://localhost:5000/createPlaylist", { ...payload })
      .then(function(response) {
        self.setState({ playlists: response.data, newPlName: "" });
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  gotoPlayList = (plId, event) => {
    event.preventDefault();
    event.stopPropagation();
    window.location.href = "/#playlist/" + plId;
  };

  render() {
    return (
      <div>
        <AppBar
          className="header"
          style={{ background: "#b5190e" }}
          title={
            <Row>
              <Col xs="3">
                <span onClick={this.gotoHome.bind(this)}>
                  <img src="assets/PlayPal.png" width="40px" /> PlayPal
                </span>
              </Col>
              {this.state.isLoggedIn ? (
                <Col xs="9">
                  <div
                    className="pull-left"
                    style={{ cursor: "pointer", marginTop: "20px" }}
                  >
                    <i
                      className="fas fa-list"
                      onClick={this.showPlaylists.bind(this)}
                    />
                  </div>
                  <div className="SearchBoxCntnr">
                    <input
                      type="text"
                      placeholder="Search"
                      onChange={this.handleChange.bind(this)}
                      defaultValue={this.state.searchparam}
                    />
                    <i
                      className="fas fa-search"
                      onClick={this.searchSongs.bind(this)}
                    />
                  </div>
                </Col>
              ) : (
                ""
              )}
            </Row>
          }
          iconStyleLeft={{ display: "none" }}
          iconElementRight={
            this.state.isLoggedIn ? (
              <div className="pull-right userInfo">
                <img className="userPic" src={this.state.user.profile_pic} />
                {this.state.user.name}
              </div>
            ) : (
              <i />
            )
          }
        />
        {!this.state.isLoggedIn ? (
          <Row>
            <Col className="text-center">
              <div className="loginCntnr">
                <h2>
                  Login to PlayPal and start sharing your favourite songs with
                  your pals
                </h2>
                <GoogleLogin
                  socialId="753402990590-0lqo733c67o0hshnjqvk5fmvkoi3rru9.apps.googleusercontent.com"
                  className="google-login"
                  scope="https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me"
                  fetchBasicProfile={true}
                  responseHandler={this.responseGoogle}
                  buttonText="Login With Google"
                  style={{
                    background: "#b5190e",
                    color: "#fff",
                    fontSize: "20px",
                    padding: "5px 20px"
                  }}
                />
              </div>
            </Col>
          </Row>
        ) : (
          <MediaWrapper
            tracks={this.state.tracks}
            playlists={this.state.playlists}
          />
        )}
        <Dialog
          modal={true}
          clasName="playListModal"
          open={this.state.showDialog}
          onRequestClose={this.handleDialogClose}
          contentStyle={{ maxHeight: "auto" }}
          actions={
            <FlatButton
              label="Cancel"
              primary={true}
              onClick={this.handleDialogClose}
            />
          }
        >
          <Row>
            <Col xs="6" className="playListCntnr">
              {this.state.playlists.owned.length > 0 ? (
                <div>
                  <h4>Your playlists</h4>
                  <div>
                    {this.state.playlists.owned.map(item => {
                      return (
                        <MenuItem
                          key={item.id}
                          primaryText={item.name}
                          onClick={this.gotoPlayList.bind(this, item.id)}
                        />
                      );
                    })}
                  </div>
                </div>
              ) : (
                <div>You haven't created any playlist yet</div>
              )}
            </Col>
            <Col xs="6" className="playListCntnr">
              {this.state.playlists.shared.length > 0 ? (
                <div>
                  <h4>Playlists shared with you</h4>
                  <div>
                    {this.state.playlists.shared.map(item => {
                      return (
                        <MenuItem
                          key={item.id}
                          primaryText={item.name}
                          onClick={this.gotoPlayList.bind(this, item.id)}
                        />
                      );
                    })}
                  </div>
                </div>
              ) : (
                <div>You don't have any shared playlist yet</div>
              )}
            </Col>
          </Row>

          <br />
          <Row>
            <Col xs="4">
              <TextField
                floatingLabelText="New playlist name"
                onChange={e => this.setState({ newPlName: e.target.value })}
                defaultValue={this.state.newPlName}
              />
            </Col>
            <Col xs="3">
              <RaisedButton
                label={
                  <span>
                    <i className="fas fa-plus" /> Create Playlist
                  </span>
                }
                primary={true}
                onClick={this.createPlayList.bind(this)}
                style={{ margin: "25px" }}
              />
            </Col>
          </Row>
        </Dialog>
      </div>
    );
  }
}

export default Main;
