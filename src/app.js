// node
// vendors
import React from "react";
// project
import Main from "./components/main";
import Playlist from "./components/playlist";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import "./styles.scss";
import { getHash } from "react-hash-route";

const componentMap = {
  main: <Main />,
  playlist: <Playlist />
};

const App = () => (
  <MuiThemeProvider>
    <div className="main-app">{componentMap[getHash() || "main"]}</div>
  </MuiThemeProvider>
);

export default App;
