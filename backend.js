var express = require('express');
var fs      = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var cors = require('cors');
const bodyParser = require('body-parser')
const store = require('store')
var app     = express();
app.use(cors());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var knex = require('knex')({
  client: 'mysql',
  connection: {
    host : 'localhost',
    user : 'root',
    password : 'root',
    database : 'playpal'
  }
});


app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.post('/createUser', async (req, res) => {
  var payload={}
  var payload = await insertRow(req.body)
  
  res.send(payload);
})

async function insertRow(params){
  let users = await knex.raw(` select * from users where email = \'`+params.email+`\'`)
  let resp;
  
  if(users[0].length==0){
    try{
      let query = `insert into users (name,email,token,accountId,isActive,profile_pic) values('${params.name}','${params.email}','${params.token}','${params.accountId}',1,'${params.profile_pic}')`;
      resp = await knex.raw(query);
      resp = await knex.raw(`select * from users where id = ${resp[0].insertId}`)
    }
    catch(err){
      console.error(err);
      resp = null
    }
  }
  else {
    resp = users
  }
  let payload = resp[0]
  
  return payload
}

app.get('/getPlaylists', async (req, res) => {
  var owned={}
  var shared={}
  owned = await knex.raw(`select * from playlist where user_id = ${req.query.userId}`)
  shared = await knex.raw(`select * from playlist where shared_id = ${req.query.userId}`)
  res.status(200).send({'owned':owned[0],'shared':shared[0]});
})
app.post('/createPlaylist', async (req, res) => {
  console.log(req.body)
  var owned={}
  var shared={}
  let resp;
  try {
    let query = `select * from playlist where user_id = '${req.body.user_id}' and name = '${req.body.name}'`
    owned = await knex.raw(query)
    if(owned[0].length==0){
      let query = `insert into playlist (name,user_id,shared_id,shared_by) values('${req.body.name}','${req.body.user_id}',null,null)`;
      resp = await knex.raw(query);
      owned = await knex.raw(`select * from playlist where user_id = ${req.body.user_id}`)
      shared = await knex.raw(`select * from playlist where shared_id = ${req.body.user_id}`)
    }
  }catch(err){
    console.log(err)
  }
  res.status(200).send({'owned':owned[0],'shared':shared[0]});
})


app.get('/getPlaylistInfo', async (req, res) => {
  var playlist={}
  var songs={}
  var status = 0;
  try{
    playlist = await knex.raw(`select * from playlist where id = ${req.query.playlist_id}`);
    if(playlist[0][0].user_id==null){
      playlist = await knex.raw(`select * from playlist where id = ${playlist[0][0].original_playlist}`);
    }
    songs = await knex.raw(`select * from songs where playlist_id = ${playlist[0][0].id}`);
    users = await knex.raw(`select * from playlist where id = ${playlist[0][0].id} and id='null' and shared_id!='null'`);
    status = 200
  }
  catch(err){
    status = 400
  }
  res.status(status).send({info:playlist[0][0],songs:songs[0],users:users[0]});
})

app.post('/addSongToPlaylist', async (req, res) => {
  let temp,buff,newsong;
  try{
    temp = await knex.raw(`select * from songs where playlist_id = ${req.body.playlist_id} and song_id = ${req.body.song_id} `);
    
    if(temp[0].length==0 && req.body.action=='add'){
      let query = `insert into songs (playlist_id,songInfo,song_id) values('${req.body.playlist_id}','${req.body.songInfo}','${req.body.song_id}')`;
      try{
        buff = await knex.raw(query.toString());
        newsong = await knex.raw(`select * from songs where id = ${buff[0].insertId}`)
      }catch(err){
        console.log(err)
      }
      
    }
  }catch(err){
    console.log(err)
  }
  if(newsong){
    res.send(newsong[0])
  }
  else{
    res.send(temp[0])
  }
})

app.post('/addSongToSharedPlaylist', async (req, res) => {
  let a,b;
  let temp,buff,newsong;
  
  
  try{
    a = await knex.raw(`select * from playlist where id = '${req.body.playlist_id}'`);
    b = await knex.raw(`select * from playlist where id = '${a[0][0].original_playlist}'`);
    try{
      temp = await knex.raw(`select * from songs where playlist_id = ${b[0][0].id} and song_id = ${req.body.song_id} `);
      
      if(temp[0].length==0 && req.body.action=='add'){
        let query = `insert into songs (playlist_id,songInfo,song_id) values('${b[0][0].id}','${req.body.songInfo}','${req.body.song_id}')`;
        try{
          buff = await knex.raw(query.toString());
          newsong = await knex.raw(`select * from songs where id = ${buff[0].insertId}`)
        }catch(err){
          console.log(err)
        }
        
      }
    }catch(err){
      console.log(err)
    }
  }catch(err){
    
  }
  
  
  if(newsong){
    res.send(newsong[0])
  }
  else{
    res.send(temp[0])
  }
})


app.post('/addUserToPlaylist', async (req, res) => {
  let user,pl;
  try{
    let query = `select * from users where email = '${req.body.email}'`
    user = await knex.raw(query.toString());
    if(user[0].length>0){
      let query = `insert into playlist (name,user_id,shared_id,shared_by,original_playlist) values('${req.body.name}',null,'${user[0][0].id}','${req.body.sharer_user_id}','${req.body.playlist_id}')`;
      try{
        pl = await knex.raw(query.toString());
      }catch(err){
        console.log(err)
      }
    }
  }catch(err){
    console.log(err)
  }
  
  res.status(200).send(pl[0]);
})


app.listen('5000')

console.log('Server running at 5000');
exports = module.exports = app;